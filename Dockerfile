FROM tensorflow/tensorflow:latest-gpu-py3

## ensure locale is set during build
ENV LANG C.UTF-8

RUN pip install keras && \
    pip install uproot && \
    pip install jupyter && \
    pip install jupyterlab && \
    pip install matplotlib && \
    pip install seaborn && \
    pip install hep_ml && \
    pip install sklearn && \
    pip install tables && \
    pip install papermill pydot
    

RUN apt-get update && \
    apt-get install -y git && \
    apt-get install -y vim less screen graphviz python3-tk
    
## run jupyter notebook by default unless a command is specified
CMD ["jupyter", "notebook", "--port", "33333", "--no-browser"]
